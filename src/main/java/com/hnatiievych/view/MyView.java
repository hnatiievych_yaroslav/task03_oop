package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImp;
import com.hnatiievych.model.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Calculate shipping cost");
        menu.put("2", "  2 - Find the fastest shipping");
        menu.put("3", "  3 - Find the cheapest shipping");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        System.out.println(" Delivery will cost " + controller.calculatePriceDelivery() + " grn");
    }

    private void pressButton2() {
        System.out.println("The fastest delivery company is "+ controller.findFastestShipping().getName());
    }

    private void pressButton3() {
        System.out.println("The cheapest  delivery company is "+ controller.findCheapestShipping().getName());
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
