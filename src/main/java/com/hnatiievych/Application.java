package com.hnatiievych;

import com.hnatiievych.model.Port;
import com.hnatiievych.view.MyView;

import java.io.FileNotFoundException;

public class Application {
    public static void main(String[] args)  {
        MyView myView = new MyView();
        myView.show();

    }
}
