package com.hnatiievych.model;

import java.util.Comparator;
import java.util.List;

public class DeliveryImp implements Delivery {
    private Cargo cargo;
    private Port portOfDeparture;
    private Port portOfArrival;
    private LogisticsСompany logisticsСompany;
    private int shippingPrice;

    public DeliveryImp() {
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public void setPortOfDeparture(Port portOfDeparture) {
        this.portOfDeparture = portOfDeparture;
    }

    public void setPortOfArrival(Port portOfArrival) {
        this.portOfArrival = portOfArrival;
    }

    public void setLogisticsСompany(LogisticsСompany logisticsСompany) {
        this.logisticsСompany = logisticsСompany;
    }

    public void setShippingPrice(int shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public DeliveryImp(Cargo cargo, Port portOfDeparture, Port portOfArrival, LogisticsСompany logisticsСompany) {
        this.cargo = cargo;
        this.portOfDeparture = portOfDeparture;
        this.portOfArrival = portOfArrival;
        this.logisticsСompany = logisticsСompany;
    }

    @Override
    public int calculatePriceDelivery() {
        this.shippingPrice = cargo.getWeight() * logisticsСompany.getPriceFor1kg();
        return shippingPrice;
    }




}
