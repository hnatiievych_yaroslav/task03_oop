package com.hnatiievych.model;


import java.io.File;
import java.io.FileNotFoundException;

import java.util.*;

public class Port {
    private String city;
    private String country;

    public Port(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public Port() {
    }

    ;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public static List<Port> readFromFile() {
        List<Port> ports = new ArrayList<Port>();
        File file = new File("src\\main\\resources\\Ports");
        try (Scanner scan = new Scanner(file)) {
            scan.useDelimiter("\\r\\n,\\D");

            while (scan.hasNextLine()) {
                String[] str = scan.nextLine().split(",");
                ports.add(new Port(str[0], str[1]));
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return ports;
    }
}


