package com.hnatiievych.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LogisticsСompany {
    //name delivery company
    private String name;
    // index speed delivery, if index bigger, company delivery cargo quily.
    private int speedShipping;
    // price for 1 kg 
    private int priceFor1kg;

    public LogisticsСompany() {
    }

    public LogisticsСompany(String name, int speedShipping, int priceFor1kg) {
        this.name = name;
        this.speedShipping = speedShipping;
        this.priceFor1kg = priceFor1kg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeedShipping() {
        return speedShipping;
    }

    public void setSpeedShipping(int speedShipping) {
        this.speedShipping = speedShipping;
    }

    public int getPriceFor1kg() {
        return priceFor1kg;
    }

    public void setPriceFor1kg(int priceFor1kg) {
        this.priceFor1kg = priceFor1kg;
    }


    public static List<LogisticsСompany> readFromFile() {
        List<LogisticsСompany> company = new ArrayList<LogisticsСompany>();
        File file = new File("src\\main\\resources\\Company");
        try (Scanner scan = new Scanner(file)) {
            scan.useDelimiter("\\r\\n,\\D");

            while (scan.hasNextLine()) {
                String[] str = scan.nextLine().split(",");
                company.add(new LogisticsСompany(str[0],
                                    Integer.parseInt(str[1]),
                                    Integer.parseInt(str[2])));
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return company;
    }
}
