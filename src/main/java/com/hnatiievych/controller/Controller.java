package com.hnatiievych.controller;

import com.hnatiievych.model.Delivery;
import com.hnatiievych.model.LogisticsСompany;

public interface Controller {


    int calculatePriceDelivery();

    LogisticsСompany findFastestShipping();

    LogisticsСompany findCheapestShipping();

    Delivery createDelivery();

}
