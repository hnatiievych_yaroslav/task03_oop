package com.hnatiievych.controller;

import com.hnatiievych.model.*;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ControllerImp implements Controller {
    private Delivery delivery;

    public ControllerImp() {
    }



    @Override
    public int calculatePriceDelivery() {
        delivery = createDelivery();
        return delivery.calculatePriceDelivery();

    }

    @Override
    public LogisticsСompany findFastestShipping() {
        List<LogisticsСompany> company = LogisticsСompany.readFromFile();
        company.sort(Comparator.comparing(LogisticsСompany::getSpeedShipping));
        return company.get(company.size() - 1);
    }

    @Override
    public LogisticsСompany findCheapestShipping() {
        List<LogisticsСompany> company = LogisticsСompany.readFromFile();
        company.sort(Comparator.comparing(LogisticsСompany::getPriceFor1kg));
        return company.get(0);
    }

    private List<Port> listOfPorts() {
        return Port.readFromFile();
    }


    private void printListPorts() {
        int index = 1;
        for (Port port : Port.readFromFile()) {
            System.out.println(index++ + " " + port.getCity() + " " + port.getCountry());
        }
    }

    private void printListDeliveryCompany() {
        int index = 1;
        for (LogisticsСompany company : LogisticsСompany.readFromFile()) {
            System.out.println(index + " " + company.getName());
            index++;
        }
    }

    private List<LogisticsСompany> listOfDeliveryCompanies() {
        return LogisticsСompany.readFromFile();
    }


    public Delivery createDelivery() {
        List<Port> ports = this.listOfPorts();
        List<LogisticsСompany> companies = this.listOfDeliveryCompanies();
        Scanner scan = new Scanner(System.in);

        System.out.println("Choose port shipping");
        this.printListPorts();
        int indexPortShipping = scan.nextInt();

        System.out.println("Choose port arriving");
        this.printListPorts();
        int indexPortArriving = scan.nextInt();

        System.out.println("enter weight of cargo");
        Cargo cargo = new Cargo();
        cargo.setWeight(scan.nextInt());

        System.out.println("enter number delivery company");
        this.printListDeliveryCompany();
        int indexDeliveryCompany = scan.nextInt();

        Delivery delivery = new DeliveryImp(cargo,
                ports.get(indexPortShipping),
                ports.get(indexPortArriving),
                companies.get(indexDeliveryCompany));

        return delivery;

    }
}